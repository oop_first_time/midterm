public class Dog {
    private String name;
    private String breed;
    private String color;
    private double price = 0;
    private double discount = 0;

    public Dog(String name, String breed, String color) {
        this.name = name;
        this.breed = breed;
        this.color = color;
    }

    public void setName(String name) { // สร้าง method ในการกำหนดชื่อ
        this.name = name;
    }

    public void setBreed(String breed) { // สร้าง method ในการกำหนดสายพันธ์
        this.breed = breed;
    }

    public void setColor(String color) { // สร้าง method ในการกำหนดสี
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public String getBreed() {
        return breed;
    }

    public String getColor() {
        return color;
    }

    public double getPrice() {
        return price;
    }

    public double getDiscount() {
        return discount;
    }

    
    

    public void print() {
        System.out.println("Your dog name:" + name + " That a great name ^^"+ " , " + "Breed: " + breed + " , " + "Color: " + color);
    }

    public double calPriceDog() {
        setBreed(breed);
        if (breed == "pomeranian") {
            int price = 22000;
            double discount = 10;
            System.out.println("Price " + price + " Bath");
            System.out.println("You get a discount!!! " + discount + " %");
            double total = price - (price * discount) / 100;
            System.out.println("Total " + total + " Bath");
            
        } if(breed == "siberian") {
            int price = 25000;
            double discount = 20;
            System.out.println("Price " + price + " Bath");
            System.out.println("You get a discount!!! " + discount + " %");
            double total = price - (price * discount) / 100;
            System.out.println("Total " + total + " Bath");
        } if (breed == "shiba") {
            int price = 45000;
            double discount = 30;
            System.out.println("Price " + price + " Bath");
            System.out.println("You get a discount!!! " + discount + " %");
            double total = price - (price * discount) / 100;
            System.out.println("Total " + total + " Bath");

        } 
        return price;

    }

  

    
}
