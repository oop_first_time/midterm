public class AppDog {
    public static void main(String[] args) {
        Dog dog1 = new Dog("Nubboon", "pomeranian", "Black");
        dog1.print();
        dog1.calPriceDog();
        System.out.println();

        Dog dog2 = new Dog("Boontang", "siberian", "grey");
        dog2.print();
        dog2.calPriceDog();
        System.out.println();


        Dog dog3 = new Dog("Kookoo", "shiba", "Red Sesame");
        dog3.print();
        dog3.calPriceDog();
        System.out.println();

    }
}
