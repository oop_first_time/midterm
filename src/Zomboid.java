import java.util.Scanner;

public class Zomboid {
    private String name;
    private String gender;
    private int age;

    public Zomboid(String name, String gender, int age) {
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public void print() {
        System.out.println("[ชื่อของคุณคือ]= " + name + " ," + "[เพศ]= " + gender + " ," + "[อายุ]= " + age);
    }

    int profession() {
        print();
        System.out.println();
        System.out.println("#===============profession==============#");
        System.out.println("#---------------------------------------#");
        System.out.println("#           1.อาชีพ: หมอ                 #");
        System.out.println("#           2.อาชีพ: ทหาร                #");
        System.out.println("#           3.อาชีพ: พนักงานดับเพลิง        #");
        System.out.println("#                                       #");
        System.out.println("#########################################");
       

        Scanner sc = new Scanner(System.in);
        System.out.println();
        System.out.print("โปรดเลือกอาชีพที่ต้องการโดยการใล่ตัวเลข: ");
        int profess = sc.nextInt();
        if (profess == 1) {
            System.out.println("[อาชีพของคุณคือ] ==> หมอ");
            System.out.println("(+)ความสามารถติดตัว = มีทักษะในการรักษา");
        } if (profess == 2) {
            System.out.println("[อาชีพของคุณคือ] ==> ทหาร");
            System.out.println("(+)ความสามารถติดตัว = มีทักษะในการเอาตัวรอด");
        } if (profess == 3) {
            System.out.println("[อาชีพของคุณคือ] ==> พนักงานดับเพลิง");
            System.out.println("(+)ความสามารถติดตัว = มีทักษะในช่วยเหลือ เเละ กล้าหาญ");
        }
        return profess;
    }

}
